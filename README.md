Berty 1.0.0
============
Minimalist theme for Datenstrom Yellow. Based on the [Basic extension](https://github.com/schulle4u/yellow-extension-basic).

## Demo

If you want to see the theme in action, head on over to my [personal website](https://www.b-erhart.de).

## Installation

[Download extension](https://gitlab.com/b-erhart/berty-theme-for-datenstrom-yellow/-/archive/master/berty-theme-for-datenstrom-yellow-master.zip) and copy zip file into your `system/extensions` folder. Right click if you use Safari.
