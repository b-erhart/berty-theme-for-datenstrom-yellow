<?php
// Berty theme, https://gitlab.com/b-erhart/berty-theme-for-datenstrom-yellow

class YellowBerty {
    const VERSION = "0.8.15";
    public $yellow;         // access to API
    
    // Handle initialisation
    public function onLoad($yellow) {
        $this->yellow = $yellow;
    }
    
    // Handle update
    public function onUpdate($action) {
        $fileName = $this->yellow->system->get("coreExtensionDirectory").$this->yellow->system->get("coreSystemFile");
        if ($action=="install") {
            $this->yellow->system->save($fileName, array("theme" => "berty"));
        } elseif ($action=="uninstall" && $this->yellow->system->get("theme")=="berty") {
            $theme = reset(array_diff($this->yellow->system->getValues("theme"), array("berty")));
            $this->yellow->system->save($fileName, array("theme" => $theme));
        }
    }
}
